import components from "./components";

const myPlugin = {
    install(Vue) {
        for(const prop of components) {
            const component = components[prop]
            Vue.component(component.name, component)
        }
    }
}

export default myPlugin